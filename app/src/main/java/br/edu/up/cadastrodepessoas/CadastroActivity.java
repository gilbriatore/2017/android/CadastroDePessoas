package br.edu.up.cadastrodepessoas;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class CadastroActivity extends AppCompatActivity {

  private EditText txtNome;
  private EditText txtSobrenome;
  private Pessoa pessoa;
  private Cadastro cadastro;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_cadastro);

    cadastro = new Cadastro(this);

    txtNome = (EditText) findViewById(R.id.txtNome);
    txtSobrenome = (EditText) findViewById(R.id.txtSobrenome);

    int id = getIntent().getIntExtra("id", -1);
    if (id > 0) {
      pessoa = cadastro.buscar(id);
      txtNome.setText(pessoa.getNome());
      txtSobrenome.setText(pessoa.getSobrenome());
    }
  }

  public void onClickGravar(View view) {

    if (txtNome.length() > 0 && txtSobrenome.length() > 0) {
      Pessoa pessoa = null;
      if (this.pessoa != null) {
        pessoa = this.pessoa;
      } else {
        pessoa = new Pessoa();
      }

      pessoa.setNome(txtNome.getText().toString());
      pessoa.setSobrenome(txtSobrenome.getText().toString());

      cadastro.gravar(pessoa);
      finish();
    } else {
      Toast.makeText(this, "Os campos são obrigatórios!", Toast.LENGTH_SHORT).show();
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    cadastro.close();
  }
}