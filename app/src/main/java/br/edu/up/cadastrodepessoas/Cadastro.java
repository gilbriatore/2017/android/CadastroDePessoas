package br.edu.up.cadastrodepessoas;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class Cadastro extends SQLiteOpenHelper {

  public Cadastro(Context context) {
    super(context, "cadastro.db", null, 1);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {

    String sql = "CREATE TABLE IF NOT EXISTS " +
        "pessoa (id integer primary key autoincrement, " +
        "nome text, sobrenome text);";

    db.execSQL(sql);
  }

  public void gravar(Pessoa pessoa) {

    ContentValues valores = new ContentValues();
    valores.put("nome", pessoa.getNome());
    valores.put("sobrenome", pessoa.getSobrenome());

    SQLiteDatabase db = getWritableDatabase();

    if (pessoa.getId() > 0) {
      String id = String.valueOf(pessoa.getId());
      db.update("pessoa", valores, "id = ?", new String[]{id});
    } else {
      db.insert("pessoa", null, valores);
    }
    db.close();
  }

  public void excluir(Pessoa pessoa){
    String id = String.valueOf(pessoa.getId());
    SQLiteDatabase db = getWritableDatabase();
    db.delete("pessoa", "id = ?", new String[]{id});
    db.close();
  }

  public Pessoa buscar(int id) {
    Pessoa pessoa = null;
    String _id = String.valueOf(id);
    SQLiteDatabase db = getReadableDatabase();
    Cursor cursor = db.query("pessoa", null, "id = ?", new String[]{_id}, null, null, null);
    if (cursor.moveToNext()) {
      pessoa = getPessoa(cursor);
    }
    cursor.close();
    return pessoa;
  }

  public ArrayList<Pessoa> listar() {

    SQLiteDatabase db = getReadableDatabase();
    Cursor cursor = db.query("pessoa", null, null, null, null, null, null);

    ArrayList<Pessoa> pessoas = new ArrayList<>();

    while (cursor.moveToNext()) {
      Pessoa pessoa = getPessoa(cursor);
      pessoas.add(pessoa);
    }
    cursor.close();

    return pessoas;
  }

  private Pessoa getPessoa (Cursor cursor){

    int idx1 = cursor.getColumnIndex("id");
    int idx2 = cursor.getColumnIndex("nome");
    int idx3 = cursor.getColumnIndex("sobrenome");

    Pessoa pessoa = new Pessoa();
    pessoa.setId(cursor.getInt(idx1));
    pessoa.setNome(cursor.getString(idx2));
    pessoa.setSobrenome(cursor.getString(idx3));

    return pessoa;
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    //Implementar quando for autualizar o banco.
  }
}